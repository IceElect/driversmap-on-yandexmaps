const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
	sequelize.define('driver_services', {
		id: {
			allowNull: false,
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.INTEGER
		},
		driverAddressId: {
			allowNull: false,
			primaryKey: true,
			type: DataTypes.INTEGER
		},
		serviceId: {
			allowNull: false,
			primaryKey: true,
			type: DataTypes.INTEGER
		},
		price: {
			allowNull: false,
			type: DataTypes.INTEGER,
		}
	}).hasOne(sequelize.models.services, {
		foreignKey: 'id',
		sourceKey: 'serviceId'
	})
};