const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
	sequelize.define('driver_addresses', {
		id: {
			allowNull: false,
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.INTEGER
		},
		driverId: {
			allowNull: false,
			primaryKey: true,
			type: DataTypes.INTEGER
		},
		address: {
			allowNull: false,
			type: DataTypes.STRING,
		},
		edited: {
			allowNull: true,
			type: DataTypes.INTEGER,
		},
		coords: {
			allowNull: false,
			type: DataTypes.STRING,
		},
		type: {
			allowNull: true,
			type: DataTypes.INTEGER,
		},
		ad: {
			allowNull: true,
			type: DataTypes.TEXT,
		}
	}).hasMany(sequelize.models.driver_services, {
		// foreignKey: 'id',
		// targetKey: 'driverAddressId'
	})
};