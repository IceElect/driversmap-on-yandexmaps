const { DataTypes } = require('sequelize');

// We export a function that defines the model.
// This function will automatically receive as parameter the Sequelize connection object.
module.exports = (sequelize) => {
	sequelize.define('drivers', {
		id: {
			allowNull: false,
			autoIncrement: true,
			primaryKey: true,
			type: DataTypes.INTEGER
		},
		name: {
			allowNull: false,
			type: DataTypes.STRING,
			// unique: true,
			// validate: {
			// 	is: /^\w{3,}$/
			// }
		},
		phone: {
			allowNull: false,
			type: DataTypes.STRING,
		},
		hose: {
			allowNull: true,
			type: DataTypes.STRING,
		},
		additional_hose: {
			allowNull: true,
			type: DataTypes.STRING,
		},
		weekend: {
			allowNull: true,
			type: DataTypes.INTEGER,
		},
		price_10km: {
			allowNull: true,
			type: DataTypes.INTEGER,
		},
		comment: {
			allowNull: true,
			type: DataTypes.TEXT,
		},
		work_status: {
			allowNull: true,
			type: DataTypes.INTEGER,
		},
		address: {
			allowNull: true,
			type: DataTypes.STRING,
		},
		lat: {
			allowNull: true,
			type: DataTypes.FLOAT,
		},
		lon: {
			allowNull: true,
			type: DataTypes.FLOAT,
		},
		status: {
			allowNull: true,
			type: DataTypes.INTEGER,
		}
	}).hasMany(sequelize.models.driver_addresses);
};