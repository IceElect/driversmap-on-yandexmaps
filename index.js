const config = require('./config');
const multer = require('multer');
const express = require('express');
const app = express();
const serv = require('http').Server(app);
const io = require('socket.io')(serv,{});
const xlsx = require('node-xlsx').default;

// const readXlsxFile = require('read-excel-file/node');
const { Sequelize, QueryTypes } = require("sequelize");

global.__basedir = __dirname;

/* DATABASE */
const Op = Sequelize.Op;
const operatorsAliases = {
  $like: Op.like,
  $not: Op.not
}
const sequelize = new Sequelize(config.db.name, config.db.user, config.db.pass, {
  dialect: "mysql",
  host: config.db.host,
  operatorsAliases
});

// var Driver = require('./models/driver.model')(sequelize);
const modelDefiners = [
	require('./models/service.model'),
	require('./models/driver_services.model'),
	require('./models/driver_addresses.model'),
	require('./models/driver.model'),
];

for (const modelDefiner of modelDefiners) {
	modelDefiner(sequelize);
}

var {models} = sequelize;
/* DATABASE END */

/* Multer Upload Storage */
const storage = multer.diskStorage({
	dest: './uploads/',
	rename: function (fieldname, filename) {
        return filename.replace(/\W+/g, '-').toLowerCase() + Date.now()
    },
	onFileUploadStart: function (file) {
        console.log(file.fieldname + ' is starting ...')
    },
    onFileUploadData: function (file, data) {
        console.log(data.length + ' of ' + file.fieldname + ' arrived')
    },
    onFileUploadComplete: function (file) {
        console.log(file.fieldname + ' uploaded to  ' + file.path)
    },
	destination: (req, file, cb) => {
		console.log(__basedir + '/uploads/');
	 cb(null, __basedir + '/uploads/')
	},
	filename: (req, file, cb) => {
	 cb(null, file.fieldname + "-" + Date.now() + "-" + file.originalname)
	}
});
 
const upload = multer({storage: storage});

/* EXPRESS */
app.use(express.static(__dirname + '/'));
app.engine('html', require('ejs').renderFile);
app.set('view engine', 'html');
app.set('views', __dirname + '/client/');

app.use(function (req, res, next) {
	res.locals = {
	    web: config.web,
	    socket: config.socket
	};
	next();
});

app.get('/', function(req, res){
    res.render('index.html',{email:'123'});
});
app.post('/import', upload.single("file"), async (req, res) => {
	console.log(1);
	var status = req.body.status || 1;
	var filePath = __basedir + '/uploads/' + req.file.filename;
	console.log(filePath);
	// importExcelData2MySQL(__basedir + '/uploads/' + req.file.filename);
	const sheet = xlsx.parse(filePath);

	var rows = sheet[0].data;
	rows.shift();

	var drivers = [];

	for (const row of rows) {
		let comment = `${row[2]}`;
			comment += ( row[6] ) ? `\n Район: ${row[6]}` : '' ;
			comment += ( row[4] && ! row[4].indexOf('не')+1 ) ? `\n Цена: ${row[4]}` : '' ;

		var addressStr = [row[5], row[6]].filter(Boolean).join(', ');
		var coords = [row[7] || '0', row[8] || '0'];

        let aDriver = {
			phone: row[0],
			name: row[1],
			address: addressStr,
			coords: coords.join(', '),
			lat: coords[0],
			lon: coords[1],
			comment,
			status
        };
        let address = {
        	ad: row[3],
        	address: addressStr,
        	coords: coords.join(', '),
        	lat: coords[0],
          	lon: coords[1],
        };
        let service = {
        	price: parseInt(row[4]),
        	serviceId: 0
        };

        var driver = await getOrCreateDriver(aDriver, {phone: {$like: aDriver.phone}})
    	address.driverId = driver.id;
		models.driver_addresses.create(address).then(async (address) => {
			service.driverAddressId = address.id;
			await upsertService(service, {driverAddressId: address.id, serviceId: 3}).then(async (service) => {
				await getAddress(address.id).then(address => {
					// send('editAddress', address[0], false, true)
					send('addAddress', address[0], false, true);
				});
			})
		})

        drivers.push(driver);
    }
	res.json({response: true});
});

app.use('/client',express.static(__dirname + '/client'));
serv.listen(process.env.PORT || 3000);
/* END EXPRESS */

function delay() {
  return new Promise(resolve => setTimeout(resolve, 2000));
}

var SOCKET_LIST = {};
var initPack = {drivers:[]};

async function addDriver(values, cb) {
	return models.drivers.create(values).then((driver) => {
		cb(driver);
	});
}

async function getDrivers() {
	// const driversOld = await models.drivers.findAll({
	// 	include: [
	// 		{
	// 			model: sequelize.models.driver_addresses,
	// 			include: [
	// 				{
	// 					model: sequelize.models.driver_services,
	// 					include: [
	// 						sequelize.models.services
	// 					]
	// 				}
	// 			]
	// 		}
	// 	]
	// });
	const drivers = await sequelize.query(
		"SELECT a.*, d.work_status, MIN(s.price) as min_price FROM drivers d LEFT JOIN driver_addresses a ON(a.driverId = d.id) LEFT JOIN driver_services s ON(s.driverAddressId = a.id) GROUP BY d.id", 
		{ type: QueryTypes.SELECT }
	);
	return drivers;
}

async function getDriver(id, cb) {
	await models.drivers.findByPk(id, {
		include: [
			{
				model: sequelize.models.driver_addresses,
				include: [
					{
						model: sequelize.models.driver_services,
						include: [
							sequelize.models.services
						]
					}
				]
			}
		]
	}).then((driver) => {
		cb(driver);
	})
	// return driver;
}

function editDriver(id, data) {
	return models.drivers.update(data, {
		where: {id}
	}).catch((error) => {
		console.error(error);
	});
}

function getOrCreateDriver(values, condition) {
	return models.drivers
	        .findOne({ where: condition })
	        .then(function(obj) {
	            // update
	            if(obj){
	                return obj;
	            }else{
		            return models.drivers.create(values);
		        }
	        })
}

/* ADDRESSES START */

async function getAddress(id) {
	const address = await sequelize.query(
		`SELECT a.*, d.work_status, d.status, ds.price as min_price, s.name as service_name FROM driver_addresses a 
			LEFT JOIN drivers d ON(a.driverId = d.id) 
			LEFT JOIN driver_services ds ON(ds.driverAddressId = a.id) 
			LEFT JOIN driver_services filter ON (filter.driverAddressId = ds.driverAddressId AND filter.price < ds.price)
			LEFT JOIN services s ON(ds.serviceId = s.id)
			WHERE a.id = ${id} AND filter.id IS NULL
			GROUP BY a.id`, 
		{ type: QueryTypes.SELECT }
	);
	return address;
}

async function getAddressses() {
	const addresses = await sequelize.query(
		`SELECT a.*, d.work_status, d.status, ds.price as min_price, s.name as service_name FROM driver_addresses a 
			LEFT JOIN drivers d ON(a.driverId = d.id) 
			LEFT JOIN driver_services ds ON(ds.driverAddressId = a.id) 
			LEFT JOIN driver_services filter ON (filter.driverAddressId = ds.driverAddressId AND filter.price < ds.price)
			LEFT JOIN services s ON(ds.serviceId = s.id)
			WHERE filter.id IS NULL
			GROUP BY a.id`, 
		{ type: QueryTypes.SELECT }
	);
	return addresses;
}

function removeAddress(id, cb) {
	models.driver_services.destroy({
		where: {driverAddressId: id}
	}).then(() => {
		models.driver_addresses.destroy({
			where: {id: id}
		}).then((res) => {
			cb();
		})
	})
}

function editAddress(id, data) {
	return models.driver_addresses.update(data, {
		where: {id}
	}).catch((error) => {
		console.error(error);
	});
}

function upsertService(values, condition) {
    return models.driver_services
        .findOne({ where: condition })
        .then(function(obj) {
            // update
            if(obj){
            	if(!values.price)
            		return obj.destroy();
            	
                return obj.update(values);
            }
            // insert
            return models.driver_services.create(values);
        })
}

/* ADDRESSES END */

io.sockets.on('connection', function(socket){
	socket.id = Math.random();
	SOCKET_LIST[socket.id] = socket;

	getAddressses().then(addresses => {
		socket.emit('init', {addresses});
	})

	socket.on('addDriver', (data, cb) => {
		addDriver(data, (driver) => {
			data.driverId = driver.id;
			models.driver_addresses.create(data).then((address) => {
				send('addAddress', address, socket.id, true);
				getDriver(driver.id, (driver) => {
					console.log(3);
					cb(driver);
				})
			})
		})
	})

	socket.on('getDriver', (id, cb) => {
		getDriver(id, (driver) => {
			driver.coords = [driver.lat, driver.lon];
			sortTariffs(driver, (tariffs) => {
				cb(driver, tariffs);
			})
		})
	})

	socket.on('editDriver', data => {
		var id = data.id;
		delete data.id;
		editDriver(id, data).then((res) => {
			data.id = id;
			send('editDriver', data, socket.id, false);
		})
	})

	socket.on('moveDriver', data => {
		editDriver(data.id, data).then((res) => {
			send('moveDriver', data, socket.id, true);
		})
	})

	socket.on('addAddress', (data, cb) => {
		models.driver_addresses.create(data).then((res) => {
			getDriver(data.driverId, (driver) => {
				res = res.dataValues;
				res.status = driver.status;
				res.work_status = driver.work_status;
				sortTariffs(driver, (tariffs) => {
					send('addAddress', res, socket.id, true);
					send('editDriver', {id: driver.id, tariffs, work_status: driver.work_status}, socket.id, false);
					cb(tariffs);
				})
			})
		})
	})

	socket.on('removeAddress', (data, cb) => {
		removeAddress(data.id, (res) => {
			getDriver(data.driverId, (driver) => {
				sortTariffs(driver, (tariffs) => {
					// cb(tariffs);
					var min_price = calcMinPrice(tariffs);
					send('removeAddress', data, socket.id, true);
					send('editDriver', {id: driver.id, tariffs, min_price, work_status: driver.work_status}, socket.id, true);
				})
			})
		})
	})

	socket.on('moveAddress', data => {
		editAddress(data.id, data).then((res) => {
			data.coords = data.coords.split(', ');
			
		})
		send('moveAddress', data, socket.id, true);
	})

	socket.on('updateService', (data, cb) => {
		upsertService(
			{price: data.price, serviceId: data.serviceId, driverAddressId: data.addressId}, 
			{serviceId: data.serviceId, driverAddressId: data.addressId}
		).then(async (res) => {
			await editAddress(data.addressId, {edited: 1}).then(result => {
				getAddress(data.addressId).then(address => {
					console.log('editAddress', address[0]);
					send('editAddress', address[0], socket.id, true)
				})
			});
			await getDriver(data.driverId, (driver) => {
				sortTariffs(driver, (tariffs) => {
					// cb(tariffs);
					var min_price = calcMinPrice(tariffs);
					send('editDriver', {id: driver.id, tariffs, min_price, work_status: driver.work_status}, socket.id, true);
				})
			})
		})
	})

	socket.on('editAddress', (data, cb) => {
		editAddress(data.addressId, data).then(result => {
			getAddress(data.addressId).then(address => {
				send('editAddress', address[0], socket.id, true)
			})
		});
	})

	socket.on('chat',function(msg){
		socket.emit('chat', msg);
	})
	socket.on('disconnect',function(){
		delete SOCKET_LIST[socket.id];
	});
})

function sortTariffs(data, cb) {
	var tariffs = {};

	models.services.findAll({order: [['weight', 'asc']]}).then((services) => {
		data.driver_addresses.forEach((address) => {
			if(!(address.id in tariffs)) {
				tariffs[address.id] = {
					id: address.id,
					driverId: address.driverId,
					address: address.address,
					coords: address.coords,
					edited: address.edited,
					type: address.type,
					min_price: 0,
					tariffs: [],
				};
			};
			var aTariffs = {};
			address.driver_services.forEach((tariff) => {
				aTariffs[tariff.serviceId] = {
					id: tariff.serviceId,
					name: tariff.service.name,
					price: tariff.price,
				};
			})
			services.forEach((service) => {
				var price = '-';
				if(service.id in aTariffs) {
					price = aTariffs[service.id].price;
					if(tariffs[address.id].min_price < price || tariffs[address.id].min_price == 0) {
						tariffs[address.id].min_price = price;
					}
				}
				tariffs[address.id].tariffs.push({
					id: service.id,
					name: service.name,
					price: price,
				});
			})
		});

		cb(tariffs);
	})
}

function calcMinPrice(tariffs) {
	var min = 0;
	for(address_id in tariffs) {
		var address = tariffs[address_id];
		address.tariffs.forEach((tariff) => {
			// if(typeof tariff.price == "number") {
				if(+tariff.price < min || min == 0)
					min = +tariff.price;
			// }
		})
	}
	return min;
}

function send(action, data, from, all) {
	for(socket_id in SOCKET_LIST) {
		if(socket_id != from || all) {
			SOCKET_LIST[socket_id].emit(action, data);
		}
	}
}

function geocode(address) {
	return new Promise((resolve, reject) => {
		request('http://www.google.com', function (error, response, body) {
			if(error) {
				reject(error);
				return false;
			}
			resolve(body);
		})
	})
}