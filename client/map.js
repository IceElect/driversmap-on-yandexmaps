var Map = function(el, socket) {
	self = {socket};
	self.currentDriver = false;
	self.addresses = {};
	self.addForm = $("form#driver-add");
	self._addForm = {
		coords: self.addForm.find('[name="coords"]'),
		address: self.addForm.find('[name="address"]'),
		phone: self.addForm.find('[name="phone"]'),
		name: self.addForm.find('[name="name"]'),
		suggest: self.addForm.find('#suggest-add-driver')
	};

	self.removedAddresses = [];

	self.init = function() {
		self.map = new ymaps.Map(el, {
            center: [55.751574, 37.573856],
            zoom: 9,
			controls: ['zoomControl', 'searchControl', 'typeSelector',  'fullscreenControl', 'rulerControl'],
			duration: 1000
        }, {
            searchControlProvider: 'yandex#search'
        });

        self.objectManager = new ymaps.ObjectManager({
            // Чтобы метки начали кластеризоваться, выставляем опцию.
            clusterize: true,
            // ObjectManager принимает те же опции, что и кластеризатор.
            gridSize: 32,
            maxZoom: [11],
            // clusterDisableClickZoom: true
            clusterHasBalloon: false,
		    // Опции геообъектов задаются с префиксом 'geoObject'.
		    geoObjectOpenBalloonOnClick: false
        });

        self.objectManager.objects.events.add('click', function (e) {
        	var objectId = e.get('objectId'); 
			var point = self.objectManager.objects['_objectsById'][objectId];

        	self.open(point.driverId, objectId, point.ad);
        	return false;
        })

        self.objectManager.objects.events.add('dragend', (e) => {
			var coords = e.get('target').geometry.getCoordinates();
			// ymaps.geocode(coords, {kind: 'house', results: 1}).then((res) => {
				// var firstGeoObject = res.geoObjects.get(0),
			        // address = firstGeoObject.getAddressLine();

			    self.socket.emit('moveAddress', {
			    	id: item.id,
			    	// address,
			    	coords: coords.join(', '),
			    	lat: coords[0],
			    	lon: coords[1],
			    })
			// })
		})

        self.objectManager.objects.options.set('preset', "islands#circleDotIcon");
        self.objectManager.objects.options.set('draggable', true);
	    self.objectManager.clusters.options.set('preset', 'islands#whiteClusterIcons');
	    self.objectManager.clusters.options.set('iconColor', '#FFFFFF');
	    self.map.geoObjects.add(self.objectManager);

	    self.popup = new Popup({
			onEdit: self.onPopupEdit,
			onClose: self.onPopupClose
		});

		self.initControls();
		self.initSuggest();
        self.initHandlers();
	}

	self.initControls = function() {
		var searchControl = self.map.controls.get('searchControl');
		searchControl.options.set('maxWidth', [30, 72, 630]);
		searchControl.options.set('fitMaxWidth', true);
		console.log(searchControl);

		var addButton = new ymaps.control.Button({
			data: {
			  content: '<i class="fal fa-plus mr-1"></i> Добавить водителя'
			},
			options: {
				maxWidth: [28, 150, 178],
			},
		}, {selectOnClick: false});

		var importButton = new ymaps.control.Button({
			data: {
			  content: '<i class="fal fa-upload mr-1"></i> Импорт водителей'
			},
			options: {
				maxWidth: [28, 150, 178],
			},
		}, {selectOnClick: false});

		self.map.controls.add(importButton);
		self.map.controls.add(addButton);

		addButton.events.add('click', function() {
			$("#addModal").modal('show')
		});
		importButton.events.add('click', function() {
			$("#importModal").modal('show')
		});
	}

	self.initSuggest = function() {
		var suggestAddAddressView = new ymaps.SuggestView('suggest-add-address');
		suggestAddAddressView.events.add('select', (e) => {
			e.preventDefault();
			let loader = $("#addAddress-loading");
			loader.show();
			var item = e.get('item');
			self.geocode(item.value, (obj) => {
				var address = item.displayName;
				var coords = obj.geometry.getCoordinates().join(', '); 
				self.socket.emit('addAddress', {
					driverId: self.currentDriver.id, 
					address,
					coords
				}, (tariffs) => {
					self.currentDriver.tariffs = tariffs;
					this.popup.renderTariffs(tariffs);
					$("#suggest-add-address").val('')
					$("#exampleModal .close").click().trigger('click');
					loader.hide();
				});
			});
			return false;
		})

		var suggestAddDriverView = new ymaps.SuggestView('suggest-add-driver');
		suggestAddDriverView.events.add('select', (e) => {
			e.preventDefault();
			var item = e.get('item');
			self.geocode(item.value, (obj) => {
				var address = item.displayName;
				var coords = obj.geometry.getCoordinates().join(', '); 

				self._addForm.coords.val(coords);
				self._addForm.address.val(address);
			});
			return false;
		})
	}

	self.initHandlers = function() {
		self.addForm.find('[type="submit"]').on('click', self.addFormSubmit);

		$(document).on('click', '[data-address-remove]', self.onRemoveAddress);

		$(document).on('change', '#importFile', self.import);
	}

	self.addPoints = (points) => {
		self.objectManager.add( map._prepareCollection(points) );
	}

	self.addPoint = (point) => {
		if(point.driverId == self.currentDriver.id) {
			self.addPoints([point])
		} else {
			self.addPoints([point])
		}
	}

	self.addFormSubmit = function(e) {
		e.preventDefault();

		var data = {};
		for(field in self._addForm) {
			var item = self._addForm[field];
			data[field] = item.val();
			self._addForm.name.removeClass('is-invalid');
		}

		var valid = true;

		if(!data.name) {
			valid = false;
			self._addForm.name.addClass('is-invalid');
		}

		if(!data.phone) {
			valid = false;
			self._addForm.phone.addClass('is-invalid');
		}

		if(!data.coords || !data.address) {
			valid = false;
			self._addForm.suggest.addClass('is-invalid');
		}

		if(valid) {

			var coords = data.coords.split(', ');
			data.lat = coords[0];
			data.lon = coords[1];

			self.socket.emit('addDriver', data, function(item) {
				$("#addModal .close").click().trigger('click');
				self.map.setCenter(coords);
				self.open(item.id);

				for(field in self._addForm) {
					self._addForm[field].val('');
				}
			});
		}

		return false;
	}

	self.move = function(id, data) {
		self.addresses[data.driverId][id].setCoords(data.coords);
	}

	self.edit = function(id, data) {
		// EDIT CURREN DRIVER IF HAS EDIT
		console.log('edit', data);
		if(id == self.currentDriver.id){
			for(var field in data) {
				self.currentDriver[field] = data[field];
			}

			this.popup.render(data);
		}

		self.renderMarkers(id, data.work_status, data.status);
	}

	self.addAddress = function(address) {
		// let point = new Point(address, 'COLLECTION', self.map);
		if(self.currentDriver.id == address.driverId) {
			let {id, work_status, status} = self.currentDriver;

			self.includeMarkers(id, work_status, status);
			self.addPoint(address);
			self.excludeMarkers(id, work_status, status);
		} else {
			self.addPoint(address);
		}
		// self.addresses[address.driverId][address.id] = point;
	}

	self.removeAddress = function(id, address) {
		var marker = self.map.geoObjects.get(id);
		console.log('remove', id);
		console.log('remove', marker);
		if(marker)
			marker.options.set('visible', false);

		delete self.addresses[address.driverId][id];
		self.removedAddresses.push(id);
		self.removedFilter();
	}

	self.onRemoveAddress = function(e) {
		var id = $(this).data('address-remove');
		var driverId = self.currentDriver.id;
		self.socket.emit('removeAddress', {id, driverId});
	}

	self.removedFilter = function() {
		self.objectManager.setFilter(function (object) {
			return ! (self.removedAddresses.indexOf(object.id) + 1);
		});
	}

	self.onPopupEdit = function(field, value) {
		var data = {
			id: self.currentDriver.id,
			[field]: value
		};
		self.currentDriver[field] = value;
		self.socket.emit('editDriver', data);

		self.renderMarkers(self.currentDriver.id, self.currentDriver.work_status, self.currentDriver.status);
	}

	self.renderMarkers = function(driver_id, work_status, status) {
		let addresses = self.addresses[driver_id];
		for(address_id in addresses) {
			let point = addresses[address_id];
			point.setStatus(status);
			point.setWorkStatus(work_status);
		}
	}

	self.open = function(id, address_id, ad) {
		var timeout = (self.currentDriver) ? 350 : 100;

		self.popup.close();
		setTimeout(() => {
			self.currentDriver = false;
			
			// GET SELECTED DRIVER DATA
			self.socket.emit('getDriver', id, (data, tariffs) => {
				data.ad = ad;
				data.tariffs = tariffs;
				self.currentDriver = data;
				// self.renderMarkers(id, data.work_status, self._preparePreset(data.id, data.status));
				
				self.excludeMarkers(id);
				self.popup.setCurrentAddress(address_id);
				self.popup.render(data);
				self.popup.open();
			})
		}, timeout);
	}

	self.onPopupClose = function(e) {
		// RESET CURRENT DRIVER MARKER
		if(self.currentDriver){
			let prevDriver = self.currentDriver;
			self.currentDriver = false;

			self.includeMarkers(prevDriver.id, prevDriver.work_status, prevDriver.status);

			self.renderMarkers(prevDriver.id, prevDriver.work_status, prevDriver.status);
		}
	}

	self.excludeMarkers = function(driverId) {
		const addresses = self.addresses[driverId];
		for(address_id in addresses) {
			let object = self.objectManager.objects['_objectsById'][address_id];
			self.objectManager.objects.remove(object);

			let point = addresses[address_id];
				point.setType('GEO');

			self.map.geoObjects.add(point.get(), address_id);
		}
	}

	self.includeMarkers = function(driverId, work_status, status) {
		let points = [];
		const addresses = self.addresses[driverId];
		for(let address_id in addresses) {
			let object = self.map.geoObjects.get(address_id);
				object.options.set('visible', false)

			let point = addresses[address_id];
				point.setType('COLLECTION');
				points.push(point.get());

		}

		let collection = {
			type: "FeatureCollection",
    		features: points
    	};
		self.objectManager.add(collection);
		// self.addPoints(points);
	}

	self.geocode = function (request, cb) {
        ymaps.geocode(request, {results: 1, kind: 'locality'}).then((res) => {
            var obj = res.geoObjects.get(0),
                error = false, hint = false;

            if (obj) {
                switch (obj.properties.get('metaDataProperty.GeocoderMetaData.precision')) {
                    case 'exact':
                        break;
                    case 'number':
                    case 'near':
                    case 'range':
                        error = 'Неточный адрес, требуется уточнение';
                        hint = 'Уточните номер дома';
                        break;
                    case 'street':
                        error = 'Неполный адрес, требуется уточнение';
                        hint = 'Уточните номер дома';
                        break;
                    case 'other':
                    default:
                        error = 'Неточный адрес, требуется уточнение';
                        hint = 'Уточните адрес';
                }
            } else {
                error = 'Адрес не найден';
                hint = 'Уточните адрес';
            }

            error = false;

            // Если геокодер возвращает пустой массив или неточный результат, то показываем ошибку.
            if (error) {
            	console.error(error);
            	console.info(hint);
            } else {
                return cb(obj);
            }
        }, function (e) {
            console.log(e)
        })

    }

    self._prepareCollection = (points) => {
    	if(!points.length) points = [points];

    	var collection = {
			type: "FeatureCollection",
    		features: []
    	};

    	points.forEach((item) => {
			collection.features.push( self._preparePoint(item) );
		})

		return collection;
	}
	
	self._preparePoint = (address) => {
		var point = new Point(address, 'COLLECTION', self.map);

    	if( !(address.driverId in self.addresses) )
    		self.addresses[address.driverId] = {}

		self.addresses[address.driverId][address.id] = point;
		return point.get();
    }

    self.import = function(e) {
		e.preventDefault();

		var btn = $(this).parent().find('.btn');
		var btnText = btn.html();
		var file = this.files[0];
		var status = $(this).closest('form').find('[name="status"]').val();

		self._request('/import', {file, status}, () => {
			btn.html('<i class="fal fa-spinner fa-spin"></i>')
		}, (response) => {
			btn.html(btnText);
			$("#importModal .close").click().trigger('click');
		})

		return false;
	}

    self._request = function(url, data, before_cb, success_cb) {
	    var form_data = new FormData();                  
	    for(key in data) {
	    	form_data.append(key, data[key]);
	    }
	    $.ajax({
	        url: url,
	        dataType: 'json',
	        cache: false,
	        contentType: false,
	        processData: false,
	        data: form_data,                         
	        type: 'post',
	        beforeSend: before_cb,
	        success: success_cb,
	        error: success_cb
	     });
	}

	self.init();

	return self;
}