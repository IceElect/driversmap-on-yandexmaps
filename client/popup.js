// TODO POPUP DATA INTERFACE

class Popup {
	el = $("#driver-popup");
	fields = {
		id: this.el.find('[data-driver="id"]'),
		status: this.el.find('[data-driver="status"]'),
		name: this.el.find('[data-driver="name"]'),
		phone: this.el.find('[data-driver="phone"]'),
		hose: this.el.find('[data-driver="hose"]'),
		additional_hose: this.el.find('[data-driver="additional_hose"]'),
		work_status: this.el.find('[data-driver="work_status"]'),
		price_10km: this.el.find('[data-driver="price_10km"]'),
		// address: this.el.find('[data-driver="address"]'),
		weekend: this.el.find('[data-driver="weekend"]'),
		comment: this.el.find('[data-driver="comment"]'),
		tariffs: this.el.find('[data-driver="tariffs"]'),
		ad: this.el.find('[data-driver="ad"]'),
	};

	params = {};
	currentAddress = 0;

	constructor(params) {
		this.params = params;

		this.reset()
		this.handlers()
	}

	handlers() {
		// POPUP EDIT HANDLERS
		for(var field in this.fields) {
			switch(field) {
				case "tariffs":
					break;
				case "address":
					break;
				case "ad":
					this.fields[field].on('keyup', this.onEditAd.bind(this));
					this.fields[field].on('change', this.onEditAd.bind(this));
					break;
				default:
					this.fields[field].on('keyup', this.onEdit.bind(this));
					this.fields[field].on('change', this.onEdit.bind(this));
					break;
			}
		}

		$('[data-driver="close"]').on('click', this.close.bind(this));

		var adToggler = $('#driverAd').parent().find('[data-toggle="collapse"]');
		$('#driverAd').on('show.bs.collapse', (e) => {adToggler.text('Скрыть')})
		$('#driverAd').on('hide.bs.collapse', (e) => {adToggler.text('Показать')})

		$(document).on('click', 'td span, th span', this.toggleServiceEdit.bind(this));
		$(document).on('keypress', 'td[data-service-id] input', this.onServiceEdit.bind(this));
		$(document).on('click', '.select-color-item:not(.close)', this.onColorEdit.bind(this));
	}

	onEdit(e) {
		var el = e.target;
		var field = $(el).attr('data-driver');
		var value = this.getDataField(field);

		this.params.onEdit(field, value)
	}

	onEditAd(e) {
		let el = e.target;
		map.socket.emit('editAddress', {addressId: this.currentAddress, ad: el.value})
	}

	setCurrentAddress(currentAddress) {
		this.currentAddress = currentAddress;
	}

	open() {
		// self.currentDriver = false;
		this.el
			.addClass('slideInUp')
			.removeClass('hidden')
			.removeClass('slideOutDown');
	}

	close(e) {
		this.el
			// .addClass('hidden')
			.addClass('slideOutDown')
			.removeClass('slideInUp');

		this.params.onClose(e);
	}

	reset() {
		for(var field in this.fields) {
			if(field == 'tariffs') {
				this.fields[field].find('th[data-service-id]').remove();
				this.fields[field].find('tbody tr').remove();
			}else if(field == 'weekend' || field == 'status'){
				this.fields[field].find('option:selected').attr('selected', false);
			}else {
				this.fields[field].val('');
				this.fields[field].html('');
			}
		}
	}

	getData() {
		var data = {};
		for(field in this.fields) {
			var value = this.getDataField(field);
			if(value !== false)
				data[field] = value;
		}

		return data;
	}

	getDataField(field) {
		switch(field) {
			case "id":
				return this.fields[field].text();
				break;
			case "tariffs":
				break;
			case "work_status":
				return (this.fields[field].is(':checked')) ? 1 : 0;
				break;

			default:
				return this.fields[field].val();
				break;
		}
		return false;
	}

	render(data) {
		for(var field in this.fields) {
			if(field in data) {
				let value = data[field];
				switch(field) {
					case "id":
						this.fields[field].text(value);
						break;
					case "tariffs":
						this.renderTariffs(value);
						break;
					case "work_status":
						this.fields[field].prop('checked', value);
						break;

					default:
						this.fields[field].val(value);
						break;
				}
			}
		}
	}

	renderTariffs(value) {
		var field = 'tariffs';
		this.fields[field].find('th[data-service-id]').remove();
		this.fields[field].find('tbody tr').remove();

		if(Object.keys(value).length) {

			// TH
			var tariffs = value[Object.keys(value)[0]].tariffs;
			tariffs.forEach((tariff) => {
				this.fields[field].find('thead tr th:last-child')
					.before('<th data-service-id="' + tariff.id + '">' + tariff.name + '</th>')
			})

			// ROWS
			for(var address_id in value) {
				var address = value[address_id];
				let point = map.addresses[address.driverId][address_id];
				let preset = point._preparePreset(true);

				let classes = [];
				if(address.edited){
					classes.push('edited');
				}
				if(address.id == this.currentAddress){
					classes.push('selected');
				}

				let selectColor = this.renderTariffsColor(preset);
				let html = `<tr class="${classes.join(' ')}" data-address-id="${address_id}">`;
					html+= `<th class="color-${preset}">
								<span>${address.address}</span>
								${selectColor}
							</th>`;

				address.tariffs.forEach((tariff) => {
					var value = (tariff.price == '-') ? '' : tariff.price;
					html += `<td data-service-id="${tariff.id}">
						<span class="d-block w-100">${tariff.price}</span>
						<div class="row shadow">
							<input class="form-control form-control-sm col-md-12 w-100" 
								data-address-id="${address_id}" 
								data-service-id="${tariff.id}" 
								value="${value}" 
							/>
							<!-- <button type="button" class="btn btn-primary btn-sm col-md-2">OK</button> -->
						</div>
					</td>`;
				})

				html += `<td><a href="#" class="text-danger" data-address-remove="${address_id}"><i class="fal fa-trash-alt"></i></a></td>`;
				html += `</tr>`;

				this.fields[field].find('tbody').append(html);
			}
		}
	}

	renderTariffsColor(selectedColor) {
		let html = `<div class="row shadow select-color" data-color="${selectedColor}">`;
		for(let status_id in STATUS_COLORS) {
			let color = STATUS_COLORS[status_id];
			let classes = ['select-color-item', `color-${color}`];
			if(selectedColor == color) 
				classes.push('selected')

			html += `<div class="${classes.join(' ')}" data-type="${status_id}" data-color="${color}"></div>`;
		}
		html += `<div class="select-color-item close" onclick="$(this).parent().removeClass('show')"></div>`;
		html += `</div>`;
		return html;
	}

	toggleServiceEdit(e) {
		let el = e.target;
		let row = $(el).closest('tr');
		let table = $(el).closest('table');
		let driverId = map.currentDriver.id;
		let addressId = row.data('address-id');

		table.find('tr.selected').removeClass('selected');
		row.addClass('selected');

		table.find('div.row.show').removeClass('show');
		$(el).parent().find('div.row').toggleClass('show');

		let point = map.addresses[driverId][addressId];
		point.navigate();
		this.render({
			ad: point.getAd()
		})
	}

	onServiceEdit(e) {
		let el = e.target;
		if (e.keyCode === 13) {
			e.preventDefault();

			var price = $(el).val();
			var driverId = map.currentDriver.id;
			var serviceId = $(el).data('service-id');
			var addressId = $(el).data('address-id');

			map.socket.emit('updateService', {price, driverId, serviceId, addressId});

			let value = (price.length) ? price : '-';
			$(el).closest('tr').addClass('edited');
			$(el).closest('td').find('span').text(value);
			$(el).closest('td').find('div.row').toggleClass('show');
			setTimeout(function() {
				$(el).closest('td').find('div.row input').focus();
			}, 500);
		}
	}

	onColorEdit(e) {
		let el = e.target;
		let holder = $(el).parent();
		let color = $(el).data('color');
		let oldClass = `color-${holder.data('color')}`;
		let newClass = `color-${color}`;
		let row = $(el).closest('tr');
		let table = $(el).closest('table');
		let driverId = map.currentDriver.id;
		let addressId = row.data('address-id');
		let type = $(el).data('type');

		map.socket.emit('editAddress', {addressId, type});

		holder.find('.selected').removeClass('selected');
		$(el).addClass('selected');

		console.log('old', oldClass);
		console.log('new', newClass);

		holder
			.removeClass('show')
			.data('color', color);

		holder.parent()
			.removeClass(oldClass)
			.addClass(newClass);
	}
}