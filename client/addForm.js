// TODO ADD FORM INTERFACE

class AddForm {
	el = $("form#driver-add");
	fields = {
		coords: this.el.find('[name="coords"]'),
		address: this.el.find('[name="address"]'),
		phone: this.el.find('[name="phone"]'),
		name: this.el.find('[name="name"]'),
		suggest: this.el.find('#suggest-add-driver')
	};

	constructor() {

	}

	onSubmit() {

	}

	validate() {

	}

	reset() {

	}
}