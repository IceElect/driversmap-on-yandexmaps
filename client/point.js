const TYPES = ['GEO', 'COLLECTION'];
const STATUS_COLORS = {
    // 1: ['gray', '#333'],
    1: 'gray',
    2: 'yellow',
    3: 'orange',
    4: 'violet',
    5: 'brown', // brown #7e5429
    6: 'darkblue', // darkblue #4a76a8
    7: 'lightblue',
    8: 'black', // dark gray #555
};

class Point {
    map;
    type = 'COLLECTION';
    driver = {};
    address = {};

    objectManager;

    constructor(address, type, ymap) {
        this.map = ymap;
        this.type = type;
        this.address = address;
        this.objectManager = map.map.geoObjects.get(0);
        this.driver = {
            id: address.driverId,
            status: address.status,
            work_status: address.work_status,
        };
    }

    get() {
        let preparers = {
            'GEO': this._prepareGeo,
            'COLLECTION': this._prepareCollection,
        };

        return preparers[this.type].bind(this)();
    }

    update(data) {
        this.address.type = data.type;
        this.address.service_name = data.service_name;

        this.setEdited(1);
        this.setMinPrice(data.min_price);
        this.setAd(data.ad);
    }

    render() {
        let {id} = this.address;

        let objectState = this.objectManager.getObjectState(id);
        if(objectState.found) {
            let object = this.objectManager.objects['_objectsById'][id];
            let properties = this._getProperties();

            if(properties) {
				// object.properties = {...object.properties, ...data.properties};
				for(let property in properties) {
					object.properties[property] = properties[property];
				}
            }

            this.objectManager.objects.setObjectOptions(id, this._getOptions());

            this.map.setZoom( this.map.getZoom() );
        } else {
            let object = self.map.geoObjects.get(id);

			if(!object) return false;

			// object.geometry.setCoordinates(data.coords)
			object.options.set(this._getOptions())
            object.properties.set(this._getProperties())
        }
    }

    edit(data) {
        let {id} = this.address;

		// let objectState = self.objectManager.getObjectState(id);
		if(this.type == 'COLLECTION') {// IF IN OBJECT MANAGER
			if(data.options) {
				self.objectManager.objects.setObjectOptions(id, data.options);
			}

			let object = self.objectManager.objects['_objectsById'][id];

			if(data.properties) {
				// object.properties = {...object.properties, ...data.properties};
				for(let property in data.properties) {
					object.properties[property] = data.properties[property];
				}
			}

			if(data.coords) {
				// self.objectManager.objects.remove(object.id);
				// self.objectManager.objects.remove(object);
				// var point = {
				// 	// id: object.id,
				// 	ad: object.ad,
				// 	geometry: {type: 'Point', coordinates: data.coords},
				// 	option: object.option,
				// 	properties: object.properties,
				// };

				// self.objectManager.add(point);
			}

			self.map.setZoom( self.map.getZoom() );
		} else { // IF IN GEO OBJECTS
			let object = self.map.geoObjects.get(id);

			if(!object) return false;

			if(data.coords) object.geometry.setCoordinates(data.coords)
			if(data.options) object.options.set(data.options)
			if(data.properties) object.properties.set(data.properties)
		}
    }

    navigate() {
        let coords = this.getCoords();
            // coords[0] = parseFloat( coords[0] ) - 0.1;
        console.log('coords', coords);
        this.map.setZoom(13, {smooth: true, duration: 1})
        this.map.setCenter( coords );
    }

    getAd() {
        return this.address.ad;
    }

    setAd(_ad) {
        if(_ad != undefined) {
            this.address.ad = _ad;
        }
    }

    setType(type) {
        this.type = type;
    }

    setEdited(edited) {
        this.address.edited = edited;
    }

    setCoords(coords) {
        this.address.coords = coords;
    }

    getCoords() {
        return this.address.coords.split(', ');
    }
    
    setStatus(status) {
        if(status) {
            this.driver.status = status;
            this.render();
        }
    }

    setWorkStatus(work_status) {
        if(work_status != undefined) {
            this.driver.work_status = !!work_status;
            this.render();
        }
    }

    setMinPrice(min_price) {
        this.address.min_price = min_price;
        this.render();
    }
    
    _getOptions() {
        let color = this._preparePreset();
        return {
            preset: `islands#grayStretchyIcon`,
            iconColor: this._preparePreset()
        }
    }

    _getProperties() {
        return {
            iconContent: this._prepareBallon(),
            clusterCaption: this._prepareBallon(),
        }
    }

    _prepareGeo() {
        let {driver, address} = this;
        let coordinates = address.coords.split(', ');
        let place = new ymaps.Placemark(
            coordinates, {
                iconContent: this._prepareBallon()
            }, {
                iconContentOffset: [8, 8],
                draggable: true,
                preset: "islands#orangeStretchyIcon",
                iconColor: 'green',
                ad: address.ad,
                address_id: address.id
            }
        );

        place.events.add('click', (e) => {
            map.open(driver.id, address.id, address.ad);
        })

        place.events.add('dragend', (e) => {
            var id = e.get('target').options.get('address_id');
            var coords = e.get('target').geometry.getCoordinates();
            // ymaps.geocode(coords, {kind: 'house', results: 1}).then((res) => {
                // var firstGeoObject = res.geoObjects.get(0),
                    // address = firstGeoObject.getAddressLine();

                map.socket.emit('moveAddress', {
                    id,
                    // address,
                    driverId: driver.id,
                    coords: coords.join(', '),
                    lat: coords[0],
                    lon: coords[1],
                })
            // })
        })

        return place;
    }

    _prepareCollection() {
        let {address} = this;
        // console.log(this);

        let coordinates = address.coords.split(', ');
        return {
			id: address.id,
			ad: address.ad,
			type: "Feature",
			driverId: address.driverId,
			geometry: {
				type: "Point",
				coordinates
            },
            options: this._getOptions(),
			properties: this._getProperties()
		}
    }

    _preparePreset = (onlyColor) => {
        let {status} = this.driver;
        let presetColor = STATUS_COLORS[status];
        
        if(this.address.type) {
            presetColor = STATUS_COLORS[this.address.type];
        }

    	if(map.currentDriver.id == this.driver.id && !onlyColor) {
    		presetColor = 'green';
    	}

    	return presetColor;
    }

    _prepareBallon = () => {
        let {work_status} = this.driver;
        let {id, min_price, service_name, edited} = this.address;

		let text = (work_status ? '*' : '') + ' ' + (
            (min_price) 
                ? `${min_price}` + ((service_name != 'другое') ? ` / <small>${service_name}</small>` : '')
                : ''
        );
		return (edited) ? `<b>${text}</b>` : `${text}`;
    }
}