var config = {};

config.db = {};
config.web = {};
config.socket = {};

config.db.host = '';
config.db.name = '';
config.db.user = '';
config.db.pass = '';

config.web.port = process.env.WEB_PORT || 3000;
config.web.title = 'SpetSuber Map';

config.socket.host = `http://localhost:${config.web.port}`;

module.exports = config;