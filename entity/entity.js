var Entity = function(){
	var self = {
		x:640,
		y:480,
		spdX:0,
		spdY:0,
		id:"",
	}
	self.update = function(){
		if(!self.updatePosition()){
			self.toRemove = true;
		}
	}
	self.updatePosition = function(){
		if(Maps.list.isPositionWall({x:self.x+self.spdX,y:self.y+self.spdY}) > 0){
			return false;
		}
		self.x += self.spdX;
		self.y += self.spdY;
		return true;
	}
	self.getDistance = function(pt){
		return Math.sqrt(Math.pow(self.x-pt.x+3,2) + Math.pow(self.y-pt.y-8,2));
	}
	return self;
}

module.exports = Entity;