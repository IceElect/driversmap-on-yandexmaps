var Entity = require('./entity');
var Player = require('./player');

var Room = function(data) {
	var self = {};
	self.players = [];
	self.timer = 0;

	self.join = function(socket_id, data) {
		self.players.push(Player(socket_id, data));
		console.log(self.players);
	}

	self.update = function() {
		self.timer++;
		console.log('update', self.players);
		return {
			timer: self.timer,
			player: self.players,
		};
	}

	return self;
}
Room.list = {};

module.exports = Room;